import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingComponent } from './common/landing/landing.component';



@NgModule({
  declarations: [LandingComponent],
  imports: [
    CommonModule
  ]
})
export class FeatureModule { }
