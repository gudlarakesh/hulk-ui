import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';

export const components: any[] = [
  LoginComponent,
  RegistrationComponent,
  HeaderComponent];

export * from './login/login.component';
export * from './registration/registration.component';
