import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as fromComponents from './components';
import { HeaderComponent } from './components/header/header.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [...fromComponents.components, HeaderComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [...fromComponents.components]
})
export class SharedModule { }
